class CartitemsController < ApplicationController

  def create
    product = Product.find(params[:product_id])
    product.add_item(current_cart, params[:quantity])
    flash[:notice] = "カートに追加しました"
    redirect_to cart_path(current_cart.id)
  end
  
  def destroy
    Cartitem.find(params[:id]).destroy
    redirect_to cart_path(current_cart.id)
    flash[:notice] = "カートから削除しました"
  end

end
