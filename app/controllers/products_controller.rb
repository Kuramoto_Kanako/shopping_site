class ProductsController < ApplicationController
  def index
    @products = Product.all
  end
  
  def new
    @products = Product.all
    @product = Product.new
  end
  
  def create
    @product = Product.new(name: params[:product][:name], price: params[:product][:price])
    if @product.save
      redirect_to root_path
    else
      render 'new'
    end
  end
  
  def destroy
    Product.find(params[:id]).destroy
    redirect_to new_product_path
  end
end
