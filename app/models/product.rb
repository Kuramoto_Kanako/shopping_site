class Product < ActiveRecord::Base
  validates_presence_of :name,  :price
  belongs_to :cart
  has_many :cartitems, dependent: :destroy
  has_many :cartitems_cart, source: :cart, through: :cartitems
  
  def add_item(cart, quantity)
    cartitems.create(cart_id: cart.id, quantity: quantity)
  end

end
