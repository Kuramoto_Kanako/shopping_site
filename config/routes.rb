Rails.application.routes.draw do
  get 'cartitems/create'

  root 'tops#main'
  get 'tops/main'

  resources :products, only: [:index, :new, :create, :destroy]
  resources :cartitems, only: [:create, :destroy]
  resources :carts, only: [:show]
end
